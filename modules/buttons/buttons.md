Présentation
------------

Le module buttons de Scampi est une copie quasi conforme de son homologue de Bootstrap, sa principale différence résidant dans le fait que les variables liées aux boutons sont rangées dans le module alors que dans Bootstrap elles sont dans le fichier général des settings.

Un prochain chantier de Scampi visera à améliorer ce module.


Utilisation
-----------

La présentation des boutons passe presque uniquement par la personnalisation des variables et l'ajout de classes sur les éléments html.

Tout le socle commun des boutons est donné par la class `btn` ; d'autres classes viennent affiner la présentation. Nous vous invitons à vous référer à la documentation spécifique de [Bootstrap concernant les boutons](http://v4-alpha.getbootstrap.com/components/buttons/).
