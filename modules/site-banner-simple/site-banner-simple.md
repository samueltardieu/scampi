Présentation
-------------

Affichage simple d’une bannière avec logo à gauche et le titre du site à côté.


Exemple d’utilisation
-------------

```` html
<div class="container">
  <div class="site-id">
    <a href="#">
      <img class="site-logo" src="scampi.png" alt="Scampi">
      <p class="site-title">Page de test Scampi et modules</p>
    </a>
  </div>
</div>
````



