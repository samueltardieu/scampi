Présentation
------------

Le module forms de Scampi est une copie quasi conforme de son homologue de Bootstrap, sa principale différence résidant dans le fait que les variables liées aux boutons sont rangées dans le module alors que dans Bootstrap elles sont dans le fichier général des settings.

Un prochain chantier de Scampi visera à améliorer ce module.


Utilisation
-----------

La présentation des éléments de formulaire passe presque uniquement par la personnalisation des variables et l’ajout de classes sur les éléments html.

Nous vous invitons à vous référer à la documentation spécifique de [Bootstrap concernant les formulaires](http://v4-alpha.getbootstrap.com/components/forms/).


Ajout Scampi
-------------

la classe `.fieldset-discrete` cache les bordures du fieldset.


note : 

- maj vers bootstrap v4.0.0-alpha.4
- deux variables et classes non reprises : select.form-control-lg select.form-control-sm
- sans prendre custom form avant audit plus poussé
